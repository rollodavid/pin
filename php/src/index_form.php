<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <?php require 'components/head.php'; ?>
        <title>Cv6</title>
    </head>
    <body>

        <header>
            <h1>Formulář</h1>
            <?php require 'components/navbar.php'; ?>
        </header>

        <main>

            <form action="index_form.php" method="post">
                <fieldset class="form-control">
                    <legend>Základní informace</legend>
                    <article>
                        <label for="id">ID:</label>
                        <input type="text" name="id" placeholder="st..." pattern="^\s*st\d{5}\s*$" title="st..." required>
                    </article>
                    <div>
                        <article>
                            <label for="student-jmeno">Jméno:</label>
                            <input type="text" name="student-jmeno" pattern="^\s*([A-Z]|[ŠČŘŽĎŤŇ])([a-z]|[ěščřžýáíéúůďťóň])+\s*$" required>
                        </article>
                        <article>
                            <label for="student-prijmeni">Příjmení:</label>
                            <input type="text" name="student-prijmeni" pattern="^\s*([A-Z]|[ŠČŘŽĎŤŇ])([a-z]|[ěščřžýáíéúůďťóň])+\s*$" required>
                        </article>
                    </div>
                </fieldset>
                <fieldset class="form-control">
                    <legend>Předměty</legend>
                    <article>
                        <label for="zkratka">Zkratky:</label>
                        <input type="text" name="zkratka" title="PIN AFJ NSQL ..." pattern="^\s*([A-Z0-9]+\s?)+\s*$" required>
                    </article>
                    <article>
                        <label for="kredity">Kredity:</label>
                        <input type="text" name="kredity" title="3 4 2 ..." pattern="^\s*((\d\s)|\d$)+\s*$" required>
                    </article>
                </fieldset>
                <fieldset class="form-control the-last-one">
                    <div>
                        <legend>Typ studia</legend>
                        <section>
                            <article>
                                <input type="radio" id="pre" name="typ-studia" value="prezencni" required>
                                <label for="pre">Prezenčně</label>
                            </article>
                            <article>
                                <input type="radio" id="dal" name="typ-studia" value="dalkove" required>
                                <label for="dal">Dálkově</label>
                            </article>
                        </section>
                    </div>
                    <div>
                        <legend>Cíl studia</legend>
                        <section>
                            <article>
                                <input type="radio" id="bs" name="cil-studia" value="bakalar" required>
                                <label for="bs">Bakalář</label>
                            </article>
                            <article>
                                <input type="radio" id="mgr" name="cil-studia" value="magistr" required>
                                <label for="mgr">Magistr</label>
                            </article>
                        </section>
                    </div>
                </fieldset>
                <input class="btn" type="submit" name="action" value="Nahrát">
            </form>

            <?php require 'index_form_logic.php'; ?>

            <h3 class="mt-5">Historie přidání:</h3>

            <form action="index_form.php" method="post">
                <input class="btn" type="submit" name="action" value="Vymazat">
            </form>

            <?php require 'index_form_logic_history.php'; ?>
            
        </main>

        <?php require 'components/footer.php'; ?>

    </body>
</html>