<!DOCTYPE html>
<html>
    <head>
        <?php require 'components/head.php'; ?>
        <title>Cv6</title>
    </head>
    <body>

        <header>
            <h1>Dolování dat</h1>
            <?php require 'components/navbar.php'; ?>
        </header>

        <main>

            <form method="post">
                <input type="text" name="xpath" required="true" placeholder="XPath dotaz">
                <input class="btn" type="submit">
            </form>

            <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    
                    $xpath = $_POST["xpath"];

                    echo '<h4 class="mt-4">' . $xpath . '</h4>';

                    echo '<table class="table text-center mt-4">
                            <thead>
                                <tr>
                                    <th>Studenti</th>
                                    <th>Odkaz</th>
                                </tr>
                            </thead>
                            <tbody>';

                    foreach (glob('studenti/*.xml') as $filename) {

                        $xml = simplexml_load_file($filename);
                        try {
                            $result = $xml->xpath($xpath);

                            if (count($result) > 0) {
                                echo '<tr>
                                        <td>' . substr($filename, stripos($filename ,'/')+1, -(strlen($filename) - stripos($filename ,'.'))) . '</td>
                                        <td><a href="weby/' . substr($filename, stripos($filename ,'/')+1, -(strlen($filename) - stripos($filename ,'.'))) . '.xml.html">více zde</a></td>
                                    </tr>';
                            }
                        } catch (Exception) {
                            echo 'Chyba';
                        }
                    }

                    echo '</tbody></table>';
                }
            ?>

        </main>

        <?php require 'components/footer.php'; ?>

    </body>
</html>