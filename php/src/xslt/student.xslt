<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8" />
                <meta name="author" content="David Rollo" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
                <link rel="stylesheet" href="../styles.css" />
                <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />
                <title>Student</title>
            </head>
            <body>

                <header>
                    <h1>
                        <xsl:value-of select="student/student-jmeno"/><xsl:text> </xsl:text><xsl:value-of select="student/student-prijmeni"/>
                    </h1>
                    <nav>
                        <a href="../index.php">Seznam studentů</a>
                        <a href="../index_xml_form.php">XML formulář</a>
                        <a href="../index_form.php">Formulář</a>
                        <a href="../index_xpath.php">Dolování</a>
                    </nav>
                </header>

                <main>

                    <h3>Studované předměty
                        <xsl:text> - </xsl:text>
                        <xsl:for-each select="student/cil-studia/*">
                            <xsl:value-of select="name()"/>
                        </xsl:for-each>
                        <xsl:text>, </xsl:text>
                        <xsl:for-each select="student/typ-studia/*">
                            <xsl:value-of select="name()"/>
                        </xsl:for-each>
                    </h3>

                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th>Zkratka</th>
                                <th>Kredity</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="student/predmety/predmet">
                                <tr>
                                    <td><xsl:value-of select="zkratka"/></td>
                                    <td><xsl:value-of select="kredity"/></td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>
                    
                </main>

                <footer>
                    <h1>Footer</h1>
                </footer>

            </body>
        </html>
    </xsl:template>

</xsl:transform>