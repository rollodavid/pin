<?php

#definice jmenného prostoru
namespace student;

#nahrani jmenneho prostoru s aliasem (import komponenty)

use DOMDocument;
use interfaces as I;

abstract class Human {

    protected $jmeno;
    protected $prijmeni;

    public function __construct($jmeno, $prijmeni)
    {
        $this->jmeno = $jmeno;
        $this->prijmeni = $prijmeni;
    }

    #abstraktni metoda, kterou musi potomek implementovat
    abstract public function vypis_informace();
}

use SimpleXMLElement;
use XSLTProcessor;

#vyzadovani jmenného prostoru pro praci (dependencies)
require 'interfaces.php';

final class Student extends Human implements I\XMLZapisovatelne {

    private $id;
    //private $predmet;
    private $zkratky;
    private $kredity;
    private $typ;
    private $cil;

    private const XML_DATABAZE_STUDENTU = 'student.xml';

    public function __construct($id, $jmeno, $prijmeni, $zkratky, $kredity, $typ, $cil) {

        parent::__construct($jmeno, $prijmeni);

        $this->id = $id;
        $this->zkratky = $zkratky;
        $this->kredity = $kredity;
        $this->typ = $typ;
        $this->cil = $cil;
    }

    #gettery (Pythonovsky typ commentu)
    function get_id() { return $this->id; }
    function get_jmeno() { return $this->jmeno; }
    function get_prijmeni() { return $this->prijmeni; }
    function get_zkratky() { return $this->zkratky; }
    function get_kredity() { return $this->kredity; }
    function get_typ() { return $this->typ; }
    function get_cil() { return $this->cil; }

    public function zapisDoXML() {

        $xml = simplexml_load_file(Student::XML_DATABAZE_STUDENTU) or die("Chyba: nelze nacist xml soubor ".Student::XML_DATABAZE_STUDENTU);;

        $informace = $xml->addAttribute("id", $this->get_id());

        $informace = $xml->addChild("student-jmeno", $this->get_jmeno());
        $informace = $xml->addChild("student-prijmeni", $this->get_prijmeni());

        $typ = $xml->addChild("typ-studia");
        $typ->addChild($this->get_typ());

        $cil = $xml->addChild("cil-studia");
        $cil->addChild($this->get_cil());

        $predmety = $xml->addChild("predmety");

        $zkratky_list = explode(" ", $this->get_zkratky());
        $kredity_list = explode(" ", $this->get_kredity());

        if (count($zkratky_list) == count($kredity_list)) {
            for ($i = 0; $i < count($zkratky_list); $i++) {
                $predmet = $predmety->addChild("predmet");
                $info = $predmet->addChild("zkratka", $zkratky_list[$i]);
                $info = $predmet->addChild("kredity", $kredity_list[$i]);
            }

            $path = 'studenti/' . $this->get_id() .'.xml';

            $xml->saveXML($path);

            // XML
            $xml_dokument = new DOMDocument();
            $xml_dokument->load($path);

            if ($xml_dokument->schemaValidate('./xsd/student.xsd')) {

                // XSL
                $xsl_dokument = new DOMDocument();
                $xsl_dokument->load("./xslt/student.xslt");

                // XSLTtransformation
                $xsl_procesor = new XSLTProcessor;
                $xsl_procesor->importStylesheet($xsl_dokument);
                $transformovany_xml = $xsl_procesor->transformToDoc($xml_dokument);
                    
                // ulozeni transformovaneho dokumentu
                $nazev_dokumentu = $this->get_id() . ".xml.html";
                $transformovany_xml->save("weby/" . $nazev_dokumentu );

            } else {
                echo '<p class="text-warning">XSD validace -> Nahraný soubor není validní! Prosím zkontrolujte správnou strukturu.</p>';
                unlink($path); //odstraneni souboru
            }
        }
        else {
            echo 'Zkratky a kredity musí mít stejný počet položek!';
        }
    }

    public function vypis_informace() {
        
        $zkratky_list = explode(" ", $this->get_zkratky());
        $kredity_list = explode(" ", $this->get_kredity());

        if (count($zkratky_list) == count($kredity_list)) {

            echo $this->get_id() . '</br>';
            echo $this->get_jmeno() . ' ';
            echo $this->get_prijmeni() . '</br>';

            for ($i = 0; $i < count($zkratky_list); $i++) {
                echo $zkratky_list[$i] . ' -> ' . $kredity_list[$i];
                if ($i != count($zkratky_list)-1) {
                    echo ', ';
                }
                else {
                    echo '</br>';
                }
            }

            echo $this->get_typ() . '</br>';
            echo $this->get_cil() . '</br>';
        }
        else {
            echo 'Zkratky a kredity musí mít stejný počet položek!';
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    switch($_POST["action"]) {
        case 'Nahrát':
            $id = trim($_POST["id"], ' ');
            $jmeno = trim($_POST["student-jmeno"], ' ');
            $prijmeni = trim($_POST["student-prijmeni"], ' ');
            $zkratky = trim($_POST["zkratka"], ' ');
            $kredity = trim($_POST["kredity"], ' ');

            $arr = array($id); //přidání nového id

            if (isset($_SESSION["id"])) {
                foreach($_SESSION["id"] as $key=>$id) {
                    array_push($arr, $id);
                }
            }

            $_SESSION["id"] = $arr;
            

            if (isset($_POST["typ-studia"]))
                $typ = $_POST["typ-studia"];

            if (isset($_POST["cil-studia"]))
                $cil = $_POST["cil-studia"];

            $st = new Student($id, $jmeno, $prijmeni, $zkratky, $kredity, $typ, $cil);

            //$st->vypis_informace();

            $st->zapisDoXML();

            break;
        case 'Vymazat':
            session_unset();
            break;
    }
}



?>