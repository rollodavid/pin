<?php
    $cookie_name = 'user';
    $cookie_value = 'David Rollo';
    setcookie($cookie_name, $cookie_value, time() + (60), "/"); //zadávají se vteřiny
    //Cookies používám na přihlášení uživatele. Lepší je použít SESSION, ale jiný příklad jsem nevymyslel.
?>

<!DOCTYPE html>
<html>
    <head>
        <?php require 'components/head.php'; ?>
    </head>
    <body>

        <header>
            <h1>Studenti</h1>
            <?php if (isset($_COOKIE[$cookie_name])) require 'components/navbar.php'; ?>
        </header>

        <main>

            <?php
                if (isset($_COOKIE[$cookie_name])) {
                    echo '<h3>Vítejte uživateli ' . $_COOKIE[$cookie_name] . '</h3>';

                    $dir = './weby';
                    $files = scandir($dir);
                    $files = array_splice($files, 2);

                    echo '<table class="table text-center">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>Odkaz</th>
                                </tr>
                            </thead>
                            <tbody>';
                    foreach ($files as $value) {
                        echo '<tr>
                                <td>' . substr($value, 0, -(strlen($value) - stripos($value ,'.'))) . '</td>
                                <td><a href="weby/' . $value . '">více zde</a></td>
                            </tr>';
                    }
                    echo '</tbody></table>';
                } else {
                    echo '<h3>Nejste přihlášen!</h3>';
                }
            ?>

        </main>

        <?php require 'components/footer.php'; ?>

    </body>
</html>