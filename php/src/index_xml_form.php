<!DOCTYPE html>
<html>
    <head>
        <?php require 'components/head.php'; ?>
        <title>Cv6</title>
    </head>
    <body>

        <header>
            <h1>XML formulář</h1>
            <?php require 'components/navbar.php'; ?>
        </header>

        <main>
            <form enctype="multipart/form-data" action="index_xml_form.php" method="POST">
                <label for="student">Kliknutím nahrajte studenta ve validním XML souboru.</label>
                <input type="file" name="student" data-max-file-size="2M"/>
                <input class="btn" type="submit">
            </form>

            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 255">
                <rect class="bc" x="0" y="0" width="200" height="255"/>
                <g>
                    <rect x="90" y="175" width="20" height="60" class="rotate25" style="fill:brown;stroke:yellow;stroke-width:1" />
                    
                    <polygon points="100,95 40,175 160,175" class="tree rotate25" />
                    <circle cx="110" cy="130" r="3" fill="red" class="rotate25" />
                    <circle cx="80" cy="150" r="3" fill="blue" class="rotate25" />
                    <circle cx="105" cy="160" r="3" fill="red" class="rotate25" />
                    <circle cx="65" cy="165" r="3" fill="red" class="rotate25" />
                    <circle cx="145" cy="165" r="3" fill="blue" class="rotate25" />
                    
                    <polygon points="100,55 60,115 140,115" class="tree rotate50" />
                    <circle cx="110" cy="90" r="3" fill="blue" class="rotate50" />
                    <circle cx="85" cy="100" r="3" fill="red" class="rotate50" />
                    
                    <polygon points="100,35 80,65 120,65" class="tree rotate75" />
                    <circle cx="100" cy="55" r="3" fill="red" class="rotate75" />
                    
                    <polygon points="100,20 93,40 112,28 88,28 107,40" class="scaleStar" style="fill:yellow;" />
                </g>
            </svg>
        </main>

        <?php require 'components/footer.php'; ?>

    </body>
</html>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $adresar_studenti = 'studenti/';
    $nahrany_student = $adresar_studenti . basename($_FILES['student']['name']);

    //if (file_exists($nahrany_recept)) {
    //  echo '<p class="text-danger">Soubor se stejným názvem již existuje v databázi. Prosím přejmenujte soubor.!</p>';
    //} else {
        if (move_uploaded_file($_FILES['student']['tmp_name'], $nahrany_student)) {

            // XSD validace
            $xml = new DOMDocument();
            $xml->load($nahrany_student);
            if ($xml->schemaValidate('./xsd/student.xsd')){
            
                echo '<p class="text-success" style="padding-bottom: 5em;">XSD validace -> Nahraný soubor je validní a byl úspěšně nahrán do databáze.</p>';
            
                // XML
                $xml_dokument = new DOMDocument();
                $xml_dokument->load($nahrany_student);

                // XSL
                $xsl_dokument = new DOMDocument();
                $xsl_dokument->load("./xslt/student.xslt");

                // XSLTtransformation
                $xsl_procesor = new XSLTProcessor();
                $xsl_procesor->importStylesheet($xsl_dokument);
                $transformovany_xml = $xsl_procesor->transformToDoc($xml_dokument);
                    
                // ulozeni transformovaneho dokumentu
                $nazev_dokumentu = basename($_FILES['student']['name']) . ".html";
                $transformovany_xml->save("weby/" . $nazev_dokumentu );

            } else {
                echo '<p class="text-warning">XSD validace -> Nahraný soubor není validní! Prosím zkontrolujte správnou strukturu.</p>';
                unlink($nahrany_student);
            }
        } else {
            echo '<p class="text-danger">Došlo k chybě při nahrávání souboru!</p>';
        }
    //}
}
?>